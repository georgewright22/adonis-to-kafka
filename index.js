const express = require('express');
const app = express();
const adonisConfig = require('./adonisConfig');
let apiConfig = require('./apiConfig');
const _async = require('async');
const CronJob = require('cron').CronJob;
const request = require('request');
const util = require('util');
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('adonis.db');
const faker = require('faker');
const moment = require('moment');
db.run('create table if not exists adonisUserStore (PIN TEXT, AlternativePIN TEXT, Picture TEXT, PictureModifiedDate TEXT, SubCompanyID TEXT, SubCompanyCode TEXT, SubCompany TEXT, hireDate TEXT, TerminationDate TEXT, ContractStartDate TEXT, ContractEndDate TEXT, isActive TEXT, DBAction TEXT, Created TEXT, Modifieddate TEXT, firstName TEXT, middleName TEXT, lastName TEXT, titleName TEXT, Rank TEXT, Nationality TEXT, Nationality3 TEXT, birthDate TEXT, birthPlace TEXT, gender TEXT, Passportnumber TEXT, PassportDateFrom TEXT, PassportExpiry TEXT, email TEXT, Mailcreated TEXT, MailModified TEXT, mobilephone TEXT, Mobilecreated TEXT, MobileModified TEXT)', function(err,results) {
    if (err) {
        console.log(err);
    }

    else {
        console.log('table exists');
    }
});

const kafka = require('kafka-node');
const client = new kafka.KafkaClient({kafkaHost: '10.9.100.157:9092, 10.9.100.142:9092, 10.9.100.37:9092'});
const Producer = kafka.Producer;
const producer = new Producer(client);
const payloads = [
    { topic: 'dev-ship-adonisToVXPOktaTest', messages: "testststststts"}
];

producer.on('ready', function (err) {
    if (err) {
        console.log("producer failed to start", err);
    }

    else {
        console.log('producer is ready.');
    }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function apolloToOkta(filter) {
    return new Promise(function(resolve,reject) {
        const options = {
            url: `https://apollosolutiononboardscheduletestapi.azurewebsites.net/api/OnBoardSchedule/EmployeeOnBoardScheduleSegments?employeeId=${filter || '1234561'}`,
            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`}
        };
    
        request.get(options, function(err,res) {
            if (err) {
                reject(err);
            }

            else {
                if (res.statusCode == 401) {
                    refreshApolloToken(apiConfig.apollo.refreshToken).then(function(accessToken) {
                        const options = {
                            url: `https://apollosolutiononboardscheduletestapi.azurewebsites.net/api/OnBoardSchedule/EmployeeOnBoardScheduleSegments?employeeId=${filter || '1234561'}`,
                            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`}
                        };                
                        request.get(options, function(err,res) {
                            if (err) {
                                reject(err);
                            }

                            else if (res.statusCode == 400) {
                                
                                resolve(null);
                            }
                
                            else {
                                const payload = JSON.parse(res.body);
                                resolve(payload);
                            }
                        });
                    });
                }
                //user doesnt exist
                else if (res.statusCode == 400) {
                    resolve(null);
                }

                else {
                    const payload = JSON.parse(res.body);
                    resolve(payload);
                }
            }
        });
    });
}

function refreshApolloToken(refreshToken) {
    return new Promise(function(resolve,reject) {
        const rp = require('request-promise');
        let apiConfig = require('./apiConfig.json');
        if (!!refreshToken) {
            const options = {
                method: 'POST',
                uri: 'https://apollosolutionauthtestapi.azurewebsites.net/api/auth/Token/CompanyRefreshToken',
                body: {
                    "clientId": apiConfig.apollo.clientID,
                    "clientSecret": apiConfig.apollo.clientSecret,
                    "companyCode": apiConfig.apollo.companyCode,
                    "refreshToken": apiConfig.apollo.refreshToken
                },
                json: true,
                headers: {"accept": "application/json"}
            };
             
            rp(options)
                .then(function (parsedBody) {
                    apiConfig.apollo.refreshToken = parsedBody.refreshToken;
                    apiConfig.apollo.accessToken = parsedBody.accessToken;
                    resolve(parsedBody.accessToken);
                })
                .catch(function (err) {
                    console.log(err);
                });
        }
    
        else {
            const options = {
                method: 'POST',
                uri: 'https://apollosolutionauthtestapi.azurewebsites.net/api/auth/Token/CompanyAuth',
                body: {
                    "clientId": apiConfig.apollo.clientID,
                    "clientSecret": apiConfig.apollo.clientSecret,
                    "companyCode": apiConfig.apollo.companyCode
                },
                json: true,
                headers: {"accept": "application/json"}
            };
             
            rp(options)
                .then(function (parsedBody) {
                    apiConfig.apollo.accessToken = parsedBody.accessToken;
                    apiConfig.apollo.refreshToken = parsedBody.refreshToken;
                    resolve(parsedBody.accessToken);
                })
                .catch(function (err) {
                    reject(err);
                });
        }
    });
}

function refreshAdonisToken() {
    return new Promise(function(resolve,reject) {
        const options = {
            url: "http://10.198.0.20/AdonisWebServices_VirginVoyagesDEV/CrewPortalWebService.svc/GNL_API_AUTHENTICATION",
            headers: {"Content-Type":"application/json"},
            body: JSON.stringify(adonisConfig)
        };

        request.post(options, function(err,res) {
            const body = JSON.parse(res.body);
            if (err) {
                console.log('err', err);
                reject(err);
            }

            else {
                apiConfig.adonis.authToken = body.GNL_API_AUTHENTICATIONResult.Authentication_Token;
                resolve(apiConfig.adonis.authToken);
            }
        });
    });
}

function getAdonisPhoto(PIN) {
    
}

function getAdonisUsers(filter) {
    return new Promise(function(resolve,reject) {
        let payloadBody = {
            "request": {
                "Authentication_Token": apiConfig.adonis.authToken,
                "View":"EmployeeInformation"
            }
        };

        if (filter) {
          payloadBody.request.Filter = "email='" + filter[1] + "'";  
        }

        let options = {
            url: "http://10.198.0.20/AIWS_VirginVoyagesDEV/AdonisIntegrationWebService.svc/GNL_APMCrewListViews",
            headers: {"Content-Type":"application/json"},
            body: JSON.stringify(payloadBody)
        };

        request.post(options, function(err,res) {
            const body = JSON.parse(res.body);
            
            if (err) {
                reject(err);
            }

            //token is expired
            else if (!body.GNL_APMCrewListViewsResult.Authentication_Approved) {
                refreshAdonisToken().then(function(token) {
                    //update body with new auth token
                    payloadBody.request.Authentication_Token = token;
                    options.body = JSON.stringify(payloadBody);
                    request.post(options, function(err,res) {
                        const body = JSON.parse(res.body);
                        if (err) {
                            reject(err);
                        }
            
                        else {
                            resolve(JSON.parse(body.GNL_APMCrewListViewsResult.Result));
                        }
                    });
                });
            }

            else {
                resolve(JSON.parse(body.GNL_APMCrewListViewsResult.Result));
            }
        });
    });
}

function getDelta(user, done) {
    db.get(`select * from adonisUserStore where PIN='${user.PIN}'`, function(err,row) {
        if (err) {
            console.log(err);
            return done(err,null);
        }
    
        else {
            if (row === undefined) {
                console.log("user does not exist");
                db.get(`insert into adonisUserStore (PIN,AlternativePIN,Picture,PictureModifiedDate,SubCompanyID,SubCompanyCode,SubCompany,hireDate,TerminationDate,ContractStartDate,ContractEndDate,isActive,DBAction,Created,Modifieddate,firstName,middleName,lastName,titleName,Rank,Nationality,Nationality3,birthDate,birthPlace,gender,Passportnumber,PassportDateFrom,PassportExpiry,email,Mailcreated,MailModified,mobilephone,Mobilecreated,MobileModified) values('${user.PIN}','${user.AlternativePIN}','${user.Picture}','${user.PictureModifiedDate}','${user.SubCompanyID}','${user.SubCompanyCode}','${user.SubCompany}','${user.hireDate}','${user.TerminationDate}','${user.ContractStartDate}','${user.ContractEndDate}','${user.isActive}','${user.DBAction}','${user.Created}','${user.Modifieddate}','${user.firstName}','${user.middleName}','${user.lastName}','${user.titleName}','${user.Rank}','${user.Nationality}','${user.Nationality3}','${user.birthDate}','${user.birthPlace}','${user.gender}','${user.Passportnumber}','${user.PassportDateFrom}','${user.PassportExpiry}','${user.email}','${user.Mailcreated}','${user.MailModified}','${user.mobilephone}','${user.Mobilecreated}','${user.MobileModified}')`, function(err,row) {
                    if (err) {
                        return done(err,null);
                    }
                
                    else {
                        console.log("ready");
                        const payload = [{ 
                            topic: 'dev-ship-adonisToVXPOktaTest', 
                            messages: JSON.stringify(user)
                        }];
                        producer.send(payload, function (err, data) {
                            if (err) {
                                console.log("err pushing new user to kafka: ", err);
                                return done(err,null);
                            }

                            else {
                                return done(null, `The following user was successfully sent to Kafka: ${user.PIN}`);
                            }
                        });
                    }
                });
            }

            else {
                for (let key in user) {
                    // skip loop if the property is from prototype
                    if (!user.hasOwnProperty(key)) continue;
                    //coerce all values to string, otherwise if a user has a null value for picture,
                    //when it is selected from sql it will be stored as a string equal to 'null', 'null' !== null
                    if (String(user[key]) == String(row[key])) {
                        continue;
                    }

                    else {
                        console.log('found an updated user', user);
                        db.get(`update adonisUserStore set PIN='${user.PIN}',AlternativePIN='${user.AlternativePIN}',Picture='${user.Picture}',PictureModifiedDate='${user.PictureModifiedDate}',SubCompanyID='${user.SubCompanyID}',SubCompanyCode='${user.SubCompanyCode}',SubCompany='${user.SubCompany}',hireDate='${user.hireDate}',TerminationDate='${user.TerminationDate}',ContractStartDate='${user.ContractStartDate}',ContractEndDate='${user.ContractEndDate}',isActive='${user.isActive}',DBAction='${user.DBAction}',Created='${user.Created}',Modifieddate='${user.Modifieddate}',firstName='${user.firstName}',middleName='${user.middleName}',lastName='${user.lastName}',titleName='${user.titleName}',Rank='${user.Rank}',Nationality='${user.Nationality}',Nationality3='${user.Nationality3}',birthDate='${user.birthDate}',birthPlace='${user.birthPlace}',gender='${user.gender}',Passportnumber='${user.Passportnumber}',PassportDateFrom='${user.PassportDateFrom}',PassportExpiry='${user.PassportExpiry}',email='${user.email}',Mailcreated='${user.Mailcreated}',MailModified='${user.MailModified}',mobilephone='${user.mobilephone}',Mobilecreated='${user.Mobilecreated}',MobileModified='${user.MobileModified}' where PIN='${user.PIN}';`, function(err,row) {
                            if (err) {
                                console.log(err);
                            }
                        
                            else {
                                console.log("test:", row);
                            }
                        });

                        const payload = [{ 
                            topic: 'dev-ship-adonisToVXPOktaTest', 
                            messages: JSON.stringify(user)
                        }];
                        producer.send(payload, function (err, data) {
                            if (err) {
                                console.log("err pushing new user to kafka: ", err);
                                return done(err,null);
                            }

                            else {
                                return done(null, `The following user was successfully sent to Kafka: ${user.PIN}`);
                            }
                        });
                    }
                }
                return done(null, 'user exists but is unchanged');
            }
        }
    });
}

function processUsers(users) {
    _async.map(users, getDelta, function(err,results) {
        if (err) {
            console.log(err);
        }

        else {
            console.log("success: ", results);
        }
    });
}

function sendRandomUserToKafka() {
    let photo = faker.fake("{{system.fileName}}").split('.');
    photo = photo[0] + '.jpg';
    const phone = `${String(Math.floor(100 + Math.random() * 900))}-${String(Math.floor(100 + Math.random() * 900))}-${String(Math.floor(1000 + Math.random() * 900))}`;
    let crew = {
        "ci": faker.fake("{{random.uuid}}"),
        "source": null,
        "ts": moment(faker.fake("{{date.recent}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
        "n": "teammember:info",
        "cn": "teammember-service",
        "tg": "teammember:updated",
        "plt": "json",
        "mrc": 3,
        "rc": 0,
        "lex": null,
        "t": null,
        "ri": 1800000,
        "k": null,
        "pl": {
            "teamMemberNumber": String(Math.floor(Math.random() * Math.floor(8974345))),
            "titleCode": "Mr.",
            "firstName": faker.fake("{{name.firstName}}"),
            "middleName": faker.fake("{{name.lastName}}"),
            "lastName": faker.fake("{{name.lastName}}"),
            "stateroom": faker.fake("{{random.number}}"),
            "birthDate": moment(faker.fake("{{date.past}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
            "citizenshipCountryCode": faker.fake("{{address.countryCode}}"),
            "gender": "M",
            "email": faker.fake("{{internet.email}}"),
            "planned": "Y",
            "estimateSignOffDate": moment(faker.fake("{{date.future}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
            "joiningDate": moment(faker.fake("{{date.past}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
            "embarkDate": moment(faker.fake("{{date.past}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
            "debarkDate": moment(faker.fake("{{date.future}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
            "crewPhoto": `Application/photos/${photo}`,
            "photoModifiedDate": moment(faker.fake("{{date.past}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
            "teamMemberUserName": faker.fake("{{internet.userName}}"),
            "teamMemberDuties": [
                {
                    "startDate": moment(faker.fake("{{date.past}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
                    "endDate": moment(faker.fake("{{date.future}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
                    "locationCode": faker.fake("{{random.number}}"),
                    "dutyLocation": faker.fake("{{lorem.word}}"),
                    "dutyName": faker.fake("{{name.jobTitle}}"),
                    "dutyCode": faker.fake("{{random.number}}")
                }
            ],
            "teamMemberRoles": [
                {
                    "teamMemberRoleName":faker.fake("{{name.jobTitle}}"),
                    "roleCode": faker.fake("{{random.number}}")
                }
            ],
            "phones": [
                {
                    "phoneTypeKey": "work",
                    "number": phone,
                    "areaCode": faker.fake("{{random.number}}"),
                    "countryCode": faker.fake("{{address.countryCode}}")
                }
            ],
            "visas": [
                {
                    "visaTypeCode":"H2A",
                    "number":faker.fake("{{random.number}}"),         
                    "issueCountryCode":faker.fake("{{address.countryCode}}"),         
                    "issueDate":moment(faker.fake("{{date.past}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),         
                    "expiryDate":moment(faker.fake("{{date.future}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),         
                    "numberOfEntries":faker.fake("{{random.number}}"),         
                    "mediaItemId":faker.fake("{{random.number}}"),         
                    "isApproved":faker.fake("{{random.boolean}}")
                }
            ],
            "wearables": [
                {
                    "wearableNumber":faker.fake("{{random.uuid}}")
                }
            ],
            "identifications": [
                {        
                    "documentTypeCode": "P",        
                    "number": faker.fake("{{random.number}}"),        
                    "issueCountryCode": faker.fake("{{address.countryCode}}"),        
                    "expiryDate": moment(faker.fake("{{date.future}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
                    "scanCopyMediaItemId": faker.fake("{{random.number}}"),
                }
            ],
            "addresses": [      
                {        
                    "addressTypeKey": "OTHER",        
                    "line1": `${faker.fake("{{address.streetAddress}}")} ${faker.fake("{{address.streetSuffix}}")}`,
                    "line2": null,        
                    "city": faker.fake("{{address.city}}"),        
                    "state": faker.fake("{{address.state}}"),        
                    "countryCode": "US",        
                    "zip": faker.fake("{{address.zipCode}}")    
                }
            ],
            "status": null
        }
    };
    
    const payload = [{ 
        topic: 'integration-ship-okta.teammember-info', 
        messages: JSON.stringify(crew)
    },
    {
        topic: 'qa-ship-okta.teammember-info', 
        messages: JSON.stringify(crew)   
    }];
    producer.send(payload, function (err, data) {
        if (err) {
            console.log("err pushing new user to kafka: ", err);
            return false;
        }

        else {
            console.log(data);
            return console.log('User successfully sent to kafka at ' + new Date());
        }
    });
    console.log(crew);
}

function sendRandomRealUser(users) {
    const userLength = users.length;
    const whichUser = Math.floor(Math.random() * (userLength) + 1);
    const chosenUser = users[whichUser];
    console.log('chosen',util.inspect(chosenUser, { maxArrayLength: null,showHidden: false, depth: null }));
    const phone = `${String(Math.floor(100 + Math.random() * 900))}-${String(Math.floor(100 + Math.random() * 900))}-${String(Math.floor(1000 + Math.random() * 900))}`;
    let crew = {
        "ci": faker.fake("{{random.uuid}}"),
        "source": null,
        "ts": moment(faker.fake("{{date.recent}}"), 'ddd MMM DD YYYY HH:mm:ss ZZ').format('YYYY-MM-DDTHH:MM:SS'),
        "n": "teammember:info",
        "cn": "teammember-service",
        "tg": "teammember:updated",
        "plt": "json",
        "mrc": 3,
        "rc": 0,
        "lex": null,
        "t": null,
        "ri": 1800000,
        "k": null,
        "pl": {
            "teamMemberNumber": chosenUser.teammemberNumber,
            "safetyNumber": chosenUser.safetyNo,
            "titleCode": chosenUser.titleCode,
            "firstName": chosenUser.firstName,
            "middleName": chosenUser.middleName || '',
            "lastName": chosenUser.lastName,
            "stateroom": chosenUser.stateroom,
            "birthDate": moment(chosenUser.birthDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
            "citizenshipCountryCode": chosenUser.citizenshipCountrycode,
            "gender": chosenUser.gender,
            "email": chosenUser.email,
            "planned": chosenUser.planned,
            "joiningDate": moment(chosenUser.joiningDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
            "embarkDate": moment(chosenUser.embarkDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
            "debarkDate": moment(chosenUser.debarkDate === '' ? '2019-07-01 00:00:00' : chosenUser.debarkDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
            "crewPhoto": chosenUser.crewPhoto,
            "photoModifiedDate": chosenUser.photoModifiedDate,
            "teamMemberUserName": chosenUser.email,
            "teamMemberDuties": [
            ],
            "teamMemberRoles": [
                {
                    "teamMemberRoleName":chosenUser.Position,
                    "roleCode": chosenUser.PositionID
                }
            ],
            //need to get phone data exposed from adonis
            "phones": [
                {
                    "phoneTypeKey": "office",
                    "number": phone,
                    "areaCode": faker.fake("{{random.number}}"),
                    "countryCode": faker.fake("{{address.countryCode}}")
                }
            ],
            "visas": [
                {
                    "visaTypeCode": chosenUser.c1d_visaTypeCode,
                    "number": chosenUser.c1d_visaNumber,
                    "issueCountryCode": chosenUser.c1d_issueCountryCode,    
                    "issueDate": chosenUser.c1d_issueDate,
                    "expiryDate": chosenUser.c1d_expiryDate, 
                    "numberOfEntries":null,         
                    "mediaItemId":null,         
                    "isApproved":null
                },
                {
                    "visaTypeCode": chosenUser.b1b2_visaTypeCode,
                    "number": chosenUser.b1b2_visaNumber,
                    "issueCountryCode": chosenUser.b1b2_issueCountryCode,    
                    "issueDate": chosenUser.b1b2_issueDate,
                    "expiryDate": chosenUser.b1b2_expiryDate,  
                    "numberOfEntries":null,         
                    "mediaItemId":null,         
                    "isApproved":null
                },
                {
                    "visaTypeCode": chosenUser.schg_visaTypeCode,
                    "number": chosenUser.schg_visaNumber,   
                    "issueCountryCode": chosenUser.schg_issueCountryCode,    
                    "issueDate": chosenUser.schg_issueDate,
                    "expiryDate": chosenUser.schg_expiryDate,   
                    "numberOfEntries":null,         
                    "mediaItemId":null,         
                    "isApproved":null
                },
                {
                    "visaTypeCode": chosenUser.dsbk_visaTypeCode,
                    "number": chosenUser.dsbk_visaNumber,   
                    "issueCountryCode": chosenUser.dsbk_issueCountryCode,    
                    "issueDate": chosenUser.dsbk_issueDate,
                    "expiryDate": chosenUser.dsbk_expiryDate, 
                    "numberOfEntries":null,         
                    "mediaItemId":null,         
                    "isApproved":null
                },
                {
                    "visaTypeCode": chosenUser.smbk_visaTypeCode,
                    "number": chosenUser.smbk_visaNumber,   
                    "issueCountryCode": chosenUser.smbk_issueCountryCode,    
                    "issueDate": chosenUser.smbk_issueDate,
                    "expiryDate": chosenUser.smbk_expiryDate, 
                    "numberOfEntries":null,         
                    "mediaItemId":null,         
                    "isApproved":null
                }
            ],
            "identifications": [
                {        
                    "documentTypeCode": "P",        
                    "number": chosenUser.pass_visaTypeCode,        
                    "issueCountryCode": chosenUser.pass_issueCountryCode,        
                    "expiryDate": chosenUser.pass_expiryDate
                }
            ],
            "addresses": [      
                {        
                    "addressTypeKey": "OTHER",        
                    "line1": chosenUser.line1,
                    "line2": chosenUser.line2 + ' ' + chosenUser.line3,        
                    "city": chosenUser.city,        
                    "state": chosenUser.state,        
                    "countryCode": chosenUser.addresscountryCode,        
                    "zip": chosenUser.zip    
                }
            ],
            "status": null
        }
    };

    apolloToOkta().then(function(data) {
        let duties = [];
        //found user
        if (data) {
            for (let i = 0; i < data.length; i++) {
                let duty = {
                    "startDate": moment(data[i].workSegmentStartTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
                    "endDate": moment(data[i].workSegmentEndTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
                    "locationCode": data[i].serviceLocationCode,
                    "dutyLocation": data[i].serviceAreaCode,
                    "dutyName": data[i].position,
                    "dutyCode": data[i].serviceAreaCode
                };
                duties.push(duty);
            }
        }
        //no user
        else {
            duties = [
                {
                    "startDate": moment(chosenUser.ContractStartDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
                    "endDate": moment(chosenUser.ContractEndDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS'),
                    "locationCode": faker.fake("{{random.number}}"),
                    "dutyLocation": faker.fake("{{lorem.word}}"),
                    "dutyName": faker.fake("{{name.jobTitle}}"),
                    "dutyCode": faker.fake("{{random.number}}")
                }
            ];
        }
        crew.pl.teamMemberDuties = duties;
        console.log('crew',util.inspect(crew, { maxArrayLength: null,showHidden: false, depth: null }));
        const payload = [{ 
            topic: 'integration-ship-okta.teammember-info', 
            messages: JSON.stringify(crew)
        },
        {
            topic: 'qa-ship-okta.teammember-info', 
            messages: JSON.stringify(crew)   
        }];
        producer.send(payload, function (err, data) {
            if (err) {
                console.log("err pushing new user to kafka: ", err);
                return false;
            }
    
            else {
                console.log(data);
                return console.log('User successfully sent to kafka at ' + new Date());
            }
        });
        //console.log('crew',util.inspect(crew, { maxArrayLength: null,showHidden: false, depth: null }));
    });
}

const main = new CronJob('0 */5 * * * *', function() {
    getAdonisUsers().then(users => processUsers(users));
});

const randomUserFeed = new CronJob('0 */5 * * * *', function() {
    sendRandomUserToKafka();
});

const realUserFeed = new CronJob('0 */3 * * * *', function() {
    getAdonisUsers().then(res => sendRandomRealUser(res));
});
getAdonisUsers().then(users => processUsers(users));
//main.start();
//randomUserFeed.start();
//realUserFeed.start();